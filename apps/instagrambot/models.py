from __future__ import unicode_literals
from django.db import models
from datetime import date

class User(models.Model):
    email = models.CharField(max_length=255, default="")
    password = models.CharField(max_length=255, default="")
    instagram_username= models.CharField(max_length=255, default="")
    instagram_password = models.CharField(max_length=255, default="")
    status = models.CharField(max_length=10, default="Inactive")
    bot_running = models.CharField(max_length = 10, default="false")
    overlay = models.CharField(max_length = 10, default="overlay")
    connect = models.CharField(max_length = 10, default="connect")
    verify = models.CharField(max_length = 10, default="hide")
    update = models.CharField(max_length = 10, default="hide")
    maxfollows = models.IntegerField(default=0)
    location = models.CharField(max_length = 120, default="NO LOCATION SET")
    using_location = models.CharField(max_length = 120, default="false")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    charge_id = models.CharField(max_length=255, default="")
    login_verified = models.CharField(max_length=6, default="false")
    update_num = models.IntegerField(default=0)
    unfollow_cycle = models.CharField(max_length=6, default="false")
    sec_code = models.IntegerField(default=0)
    proxy = models.CharField(max_length = 32, default="")
    proxy_port = models.IntegerField(default=0)


class Hashtag(models.Model):
    hashtag_name = models.CharField(max_length=255)
    users_hashtag = models.ForeignKey(User, related_name='hashtags', null=True)    

class Keyword(models.Model):
    keyword_name = models.CharField(max_length=255)
    users_keyword = models.ForeignKey(User, related_name='keywords', null=True)    

class Blacklist(models.Model):
    blacklist_name = models.CharField(max_length=255)
    users_blacklist = models.ForeignKey(User, related_name='blacklists', null=True)    

class Interest(models.Model):
    interest_name = models.CharField(max_length= 100)
    users_interest = models.ForeignKey(User, related_name='interests', null=True)

class Usernames_Follower(models.Model):
    usernames_follower_name = models.CharField(max_length=50)
    users_usernames_follower = models.ForeignKey(User, related_name='usernames_followers', null=True)

class Unfollow(models.Model):
    unfollow_user_profile = models.CharField(max_length=255)
    users_unfollow = models.ForeignKey(User,related_name="unfollow_list", null=True)