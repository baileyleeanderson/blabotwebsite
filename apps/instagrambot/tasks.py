from selenium import webdriver
from django.db import models
from models import *
from selenium.webdriver.common.keys import Keys
from selenium.common import exceptions
from selenium.webdriver.common.proxy import Proxy, ProxyType
import time
import random
from background_task import background
from celery import shared_task
from proxies import rotate_proxy as rotate_proxy

@shared_task
def payment_verified_running_bot(user, user_id):
    print('payment_verifed_starting_bot')
    hashtags = []
    keywords = []
    blacklist = []
    usernames = []
    user_hashtags = user.hashtags.all()
    user_keywords = user.keywords.all()
    user_blacklist = user.blacklists.all()
    user_usernames = user.usernames_followers.all()

    if (user.using_location == "true"):
        user_searchtype = "location"
    elif (user.unfollow_cycle == "true"):
        user_searchtype = "unfollow_cycle"
    elif (len(user_usernames) >= 1):
        user_searchtype = "usernames"
    else:
        user_searchtype = "hashtag"
    for h in user_hashtags:
        hashtags.append(h.hashtag_name)
    for k in user_keywords:
        keywords.append(k.keyword_name)
    for b in user_blacklist:
        blacklist.append(b.blacklist_name)
    for u in user_usernames:
        usernames.append(u.usernames_follower_name)

    runBot = InstagramBot (
        user.instagram_username, 
        user.instagram_password,          
        hashtags, 
        keywords, 
        user.location, 
        user.using_location,
        user.maxfollows, 
        blacklist,
        user_searchtype, 
        user_id,
        user.update_num,
        usernames
    ) 
    
    runBot.login()
    user.bot_running = "false"
    user.save()
    return 

class InstagramBot:

    def __init__(self, username, password, hashtags, keywords, location, using_location, maxfollows, blacklist, searchtype, user_id, update_num,usernames):
        self.username = username
        self.password = password
        self.hashtags = hashtags
        self.keywords = keywords
        self.location = location
        self.using_location = using_location
        self.maxfollows = maxfollows
        self.blacklist = blacklist
        self.searchtype = searchtype
        self.user_id = user_id
        self.update_num = update_num
        self.usernames = usernames
        self.pic_hrefs = []
        self.pics_liked = 0
        self.follow_this_username = False

        user = User.objects.get(id=self.user_id)
        if len(user.proxy) <= 2:
            user = User.objects.get(id=self.user_id)
            new_proxy = rotate_proxy(self.user_id)
            user.proxy = str(new_proxy[0])
            print("saving user proxy " + user.proxy)
            user.proxy_port = new_proxy[1]
            user.save()
        print("user proxy " + user.proxy)
        options = webdriver.ChromeOptions()
        options.add_argument("user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/" + str(user.proxy) +" Safari/537.36")
        options.add_argument("--disable-infobars")
        options.add_argument("--window-size=1920,1080")
        options.add_argument("--start-maximized")
        options.add_argument("--headless")
        PROXY = str(user.proxy)+ ":" + str(user.proxy_port) # IP:PORT or HOST:PORT
        options.add_argument('--proxy-server=%s' % PROXY)
        self.driver = webdriver.Chrome(chrome_options=options)
        self.driver.delete_all_cookies()

    def closeBrowser(self):
        user = User.objects.get(id= self.user_id)
        user.update = "hide"
        user.overlay = "hide"
        user.bot_running = "false"
        user.save()
        self.driver.close()
        return

    def send_verification_code(self):
        verification_form = self.driver.find_element_by_xpath("//input[@name='security_code']")
        verification_form.clear()
        secs = 90
        num = 0
        while num <= secs:
            time.sleep(1)
            num += 1
            user = User.objects.get(id=self.user_id)
            if user.sec_code == 0:
                continue
            else:
                verification_form = self.driver.find_element_by_xpath("//input[@name='security_code']")
                verification_form.clear()
                verification_form.send_keys(user.sec_code)
                time.sleep(1)
                submit_button = self.driver.find_element_by_xpath("/html/body/span/section/div/div/div[2]/form/span/button")
                submit_button.click()
                break
        if user.sec_code == 0:
            user = User.objects.get(id=self.user_id)
            user.login_verified = "false"
            self.closebrowser()
            return
            
    def check_for_login_or_verify_button(self):
        buttons_in_view = self.driver.find_elements_by_tag_name('button')
        num = 0
        for b in buttons_in_view:
            print(str(num) + b.text)
            num += 1
            if "Log in" in b.text:
                user = User.objects.get(id=self.user_id)
                user.login_verified = "false"
                user.verify = "hide"
                user.save()
                print("not logged in")
                return
            elif "Send Security Code" in b.text:
                user = User.objects.get(id=self.user_id)
                user.login_verified = "secure"
                user.verify = "verify"
                user.save()
                print("need verify")
                b.click()
                time.sleep(1)
                self.send_verification_code()
                return
            else:
                continue 
        user = User.objects.get(id=self.user_id)
        user.login_verified = "true"
        user.save()    

    def check_blacklist(self, post_or_username):
        print("in blacklist")
        print("post_or_username " + str(post_or_username))
        for b in self.blacklist:
            if b not in post_or_username:
                continue
            else:
                return True
        return False  

    def keyword_check(self, username_or_post):
        for key in self.keywords:
            if key not in username_or_post:
                continue
            else:
                return True
        return False

    def login(self):
        print("login function")
        driver = self.driver
        try:
            driver.get("https://www.instagram.com/accounts/login/?source=auth_switcher")
            time.sleep(1)
            user_name_elem = driver.find_element_by_xpath("//input[@name='username']")
            user_name_elem.clear()
            user_name_elem.send_keys(self.username)
            password_elem = driver.find_element_by_xpath("//input[@name='password']")
            password_elem.clear()
            password_elem.send_keys(self.password)
            time.sleep(1)
        except Exception:
            user = User.objects.get(id=self.user_id)
            user.proxy = ""
            user.port = 0
            user.save()
            self.closeBrowser()
            return
        buttons_in_view = driver.find_elements_by_tag_name('button')
        for b in buttons_in_view:
            if(b.text == "Log In"):
                b.click()
            else:
                continue
        time.sleep(2)
        self.check_for_login_or_verify_button()
        time.sleep(1)
        user = User.objects.get(id=self.user_id)
        if user.login_verified == "false":
            self.closeBrowser()
            return
        if user.login_verified == "secure":
            self.check_for_login_or_verify_button()
        
        user.overlay = "overlay"
        user.update = "update"
        user.bot_running = "true"
        user.save()   
        if self.searchtype == "verify":
            self.closeBrowser()
            return
        elif self.searchtype == "hashtag":
            self.gathering_photos_for_hashtag_likes()
        elif self.searchtype == "location":
            self.gather_photos_for_geo_location()
        elif self.searchtype == "usernames":
            self.username_followers()
        elif self.searchtype == "unfollow_cycle":
            self.unfollow_cycle()

    def gathering_photos_for_hashtag_likes(self):
        print("gathering photos")
        driver = self.driver
        self.pic_hrefs = []
        for hashtag in range(len(self.hashtags)):
            user = User.objects.get(id= self.user_id)
            if(user.bot_running == "false"):
                self.closeBrowser()
                return
            if(self.update_num != user.update_num):
                self.closeBrowser()
                return
            driver.get("https://www.instagram.com/explore/tags/" + self.hashtags[hashtag] + "/")
            time.sleep(2)
            buttons_in_view = driver.find_elements_by_tag_name('button')
            print("buttons_in_view " + str(len(buttons_in_view)))
            for i in range(0, 500): #page scroll range to gather photos
                user = User.objects.get(id= self.user_id)
                if(user.bot_running == "false"):
                    self.closeBrowser()
                    return
                if(self.update_num != user.update_num):
                    self.closeBrowser()
                    return
                try:
                    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
                    time.sleep(2)
                    hrefs_in_view = driver.find_elements_by_tag_name('a')
                    print ("hrefs " + str(len(hrefs_in_view)))
                    hrefs_in_view = [elem.get_attribute('href') for elem in hrefs_in_view]                                
                    only_add_picture_a_tags = "https://www.instagram.com/p"
                    for href in hrefs_in_view:
                        if only_add_picture_a_tags not in href:
                            continue
                        else:
                            if href not in self.pic_hrefs:
                                self.pic_hrefs.append(href)
                    print("Check: pic href length " + str(len(self.pic_hrefs)))
                except Exception:
                    continue
                print ("Amount of Pics Gathered " + str(len(self.pic_hrefs)))
        self.pics_liked = 0        
        self.like_and_follow_pic_href(self.pic_hrefs)

    def like_and_follow_pic_href(self,pic_hrefs): 
        print("in like and follow pic href")
        driver = self.driver
        total_pics_liked = 0
        accounts_followed = 0      
        for pic_href in pic_hrefs:
            user = User.objects.get(id=self.user_id)
            if(user.bot_running == "false"):
                self.closeBrowser()
                return
            if(self.update_num != user.update_num):
                self.closeBrowser()
                return
            try:
                driver.get(pic_href)
                time.sleep(2)
                follow_button = driver.find_elements_by_tag_name("button")
                follow_button_text = follow_button[0].text
                if (follow_button_text == "Following"):
                    continue
                #this is where I might need a scroll up to find user post
                like_bttn = driver.find_elements_by_xpath('/html/body/span/section/main/div/div/article/div[2]/section[1]/span[1]/button')
                aria_label = like_bttn[0].find_element_by_css_selector('span').get_attribute("aria-label")
                if (aria_label == "Unlike"):
                    continue
                posts_username = driver.find_element_by_xpath("//span[@id='react-root']/section/main/div/div/article/header/div[2]/div[1]/div[1]/h2/a")
                print("username " + posts_username.text)
                user = User.objects.get(id=self.user_id)
                users_blacklists = user.blacklists.all()
                if len(users_blacklists) >=1:
                    blacklisted = self.check_blacklist(posts_username.text)
                    if blacklisted == True:
                        continue
                #time sleep for instagram limits
                time.sleep(random.randint(135,160))
                user_post = driver.find_element_by_xpath("//div[@class='C4VMK']/span")
                user_post_text = user_post.text
                print("user post " + user_post_text)
                user = User.objects.get(id=self.user_id)
                users_keywords = user.keywords.all()
                if len(users_keywords) >=1:
                    keyword_found = self.keyword_check(user_post_text)
                    if keyword_found == True or self.follow_this_username == True:    
                        follow_button = driver.find_elements_by_tag_name("button")
                        follow_button_text = follow_button[0].text
                        print('followtext for keyword found ' + follow_button_text)
                        if (follow_button_text == "Follow"):
                            if (self.maxfollows >= 1):
                                follow_button[0].click()
                                new_unfollow = Unfollow.objects.create(unfollow_user_profile=pic_href,users_unfollow=user)
                                accounts_followed += 1
                                print ("Followed " + str(accounts_followed) + " accounts")
                                self.pics_liked = 0
                                self.maxfollows -= 1
                                self.follow_this_username = False
                elif (self.pics_liked == 4):
                    follow_button = driver.find_elements_by_tag_name("button")
                    follow_button_text = follow_button[0].text
                    print('followtext ' + follow_button_text)
                    if (follow_button_text == "Follow"):
                        if (self.maxfollows >= 1):
                            follow_button[0].click()
                            new_unfollow = Unfollow.objects.create(unfollow_user_profile=pic_href,users_unfollow=user)
                            accounts_followed += 1
                            print ("Followed " + str(accounts_followed) + " accounts")
                            self.pics_liked = 0
                            time.sleep(2)
                            self.maxfollows -= 1
                aria_label = like_bttn[0].find_element_by_css_selector('span').get_attribute("aria-label")
                if aria_label == "Like":
                    print ("Liked " + str(total_pics_liked) + " pics")
                    # like_button = lambda: driver.find_element_by_xpath('/html/body/span/section/main/div/div/article/div[2]/section[1]/span[1]/button').click()
                    like_bttn = driver.find_elements_by_xpath('/html/body/span/section/main/div/div/article/div[2]/section[1]/span[1]/button')
                    like_bttn[0].click() 
                    self.pics_liked += 1 
                    total_pics_liked += 1
            except Exception:
                continue
        if self.searchtype == "usernames":
            return    
        self.closeBrowser()
        return      

    def gather_photos_for_geo_location(self):
        driver = self.driver
        driver.get("https://www.instagram.com/explore/locations/")
        time.sleep(2)
        searchbar = driver.find_element_by_xpath("html/body/span/section/nav/div[2]/div/div/div[2]/input")
        searchbar.clear()
        searchbar.send_keys(self.location)
        time.sleep(2)
        location_link = driver.find_element_by_xpath("html/body/span/section/nav/div[2]/div/div/div[2]/div[2]/div[2]/div/a[1]")
        time.sleep(1)
        location_link.send_keys(u'\ue007')
        time.sleep(2)
        self.pic_hrefs = []

        for i in range(0,500):#scroll to gather photos
            user = User.objects.get(id= self.user_id)
            if(user.bot_running == "false"):
                self.closeBrowser()
                return
            if(self.update_num != user.update_num):
                self.closeBrowser() 
                return 
            try: 
                driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
                time.sleep(2)
                hrefs_in_view = driver.find_elements_by_tag_name('a')
                hrefs_in_view = [elem.get_attribute('href') for elem in hrefs_in_view]
                for href in hrefs_in_view:
                    self.pic_hrefs.append(href)
            except Exception:
                continue
        self.pics_liked = 0
        self.like_and_follow_pic_href(self.pic_hrefs)
        return

    def username_followers(self):
        self.pics_liked = 0
        users_sliced = []
        all_users_sliced = []
        user = User.objects.get(id= self.user_id)
        for username in self.usernames:
            if(user.bot_running == "false"):
                self.closeBrowser()
                return 
            if(self.update_num != user.update_num):
                self.closeBrowser()
                return
            try:
                driver = self.driver
                driver.get("https://www.instagram.com/" + username + "/")
                time.sleep(2)
                followers_button = driver.find_element_by_xpath("/html/body/span/section/main/div/header/section/ul/li[2]/a/span")
                followers_button.click()
                time.sleep(2)   
                all_users = []         
                users_to_follow = driver.find_elements_by_tag_name("a")
                dont_add_tags_that_contain = "https://www.instagram.com/p/"
                dont_add_tag = "https://www.instagram.com/explore"
            except Exception:
                continue

            for i in range (0, 200):#range to gather username followers
                user = User.objects.get(id= self.user_id)
                if(user.bot_running == "false"):
                    self.closeBrowser() 
                    return
                if(self.update_num != user.update_num):
                    self.closeBrowser()
                    return
                try:
                    followers_elem_popup = driver.find_element_by_class_name('oMwYe')
                    followers_elem_popup.click()
                    time.sleep(2)
                    users_to_follow = driver.find_elements_by_tag_name("a")
                    users_to_follow = [elem.get_attribute('href') for elem in users_to_follow]
                    dont_add_tags_that_contain = "https://www.instagram.com/p/"
                    dont_add_explore_tags = "https://www.instagram.com/explore/"
                    for user in users_to_follow:
                        if dont_add_tags_that_contain in user:
                            continue
                        elif dont_add_explore_tags in user:
                            continue
                        elif user not in all_users:
                            all_users.append(user)
                except Exception:
                    continue
            try:
                users_sliced = all_users[22:]
                for user in users_sliced:
                    all_users_sliced.append(user)
                for a in self.pic_hrefs:
                    print(str(a))
            except Exception:
                continue

        for follower in all_users_sliced:
            if(user.bot_running == "false"):
                self.closeBrowser() 
                return
            if(self.update_num != user.update_num):
                self.closeBrowser()
                return
            print('made it to all_users_sliced')
            blacklisted = False
            follow_this_user = False
            self.follow_this_username = False
            user = User.objects.get(id= self.user_id)
            print("follower " + follower)
            driver.get(follower)
            time.sleep(2)
            try:
                users_bio = driver.find_element_by_xpath("//div[@class='-vDIg']/span")
                users_bio_text = users_bio.text.encode("ascii", "ignore")
                print("users bio " + str(users_bio_text))
                key_found = self.keyword_check(str(users_bio_text))
                if key_found == True:
                    self.follow_this_username = True
            except Exception:
                print('failed')
            time.sleep(2)
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            time.sleep(1)
            users_posts = driver.find_elements_by_tag_name('a')
            users_posts = [elem.get_attribute('href') for elem in users_posts]
            first_post = []
            first_post.append(users_posts[4])
            self.like_and_follow_pic_href(first_post)
        self.searchtype = "hashtag"
        self.gathering_photos_for_hashtag_likes()  
        return      
#_______________________________________________________________________________

    def unfollow_cycle(self):
        user = User.objects.get(id= self.user_id)
        num_accounts_unfollowed = 0
        unfollow_list = user.unfollow_list.all()
        driver = self.driver
        for account in unfollow_list:
            user = User.objects.get(id= self.user_id)
            if(user.bot_running == "false"):
                self.closeBrowser()
                return 
            if(self.update_num != user.update_num):
                self.closeBrowser()
                return
            try:
                driver.get(account.unfollow_user_profile)
                time.sleep(2)
                visible_buttons = driver.find_elements_by_tag_name("button")
                unfollow_button = visible_buttons[0]
                if num_accounts_unfollowed >= 185:
                    break
                if unfollow_button.text == "Following":
                    time.sleep(random.randint(59, 123))
                    time.sleep(random.randint(1, 2))
                    unfollow_button.click()
                    time.sleep(2)
                    unfollow_popup_button = driver.find_element_by_xpath("//div[@class='mt3GC']/button")
                    unfollow_popup_button.click()
                    delete_user_url = Unfollow.objects.get(id=account.id)
                    delete_user_url.delete()
                    num_accounts_unfollowed += 1
                else:
                    delete_user_url = Unfollow.objects.get(id=account.id)
                    delete_user_url.delete()
                    continue
            except Exception:
                continue        
        if (user.using_location == "true"):
            self.gather_photos_for_geo_location()
            return
        elif (len(self.usernames) >= 1 ):
            self.username_followers()
            return
        else:
            self.gathering_photos_for_hashtag_likes()
            return
