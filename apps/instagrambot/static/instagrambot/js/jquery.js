
$(document).ready(function() {
    $('#active_button').click(function() {
        $('.active_users_table').show(),
        $('.inactive_users_table').hide(),
        $('.admin_users_table').hide()
    })
    $('#inactive_button').click(function() {
        $('.active_users_table').hide(),
        $('.inactive_users_table').show()
        $('.admin_users_table').hide()
    })
    $('#admin_button').click(function() {
        $('.active_users_table').hide(),
        $('.inactive_users_table').hide(),
        $('.admin_users_table').show()
    })
    $('#connect_button').click(function() {
        $('#connect_button').hide(),
        $('#loading').show()
    })
    $('#forgot_button').click(function() {
        $('#forgot_password').show(),
        $('#pricing_form').hide()
    })
    $('#forgot_button').mouseenter(function() {
        $(this).css("color","black")
    })
    $('#need_button').mouseenter(function() {
        $(this).css("color","black")
    })
    $('#sec_code_button_green').click(function() {
        $('#sec_code_button_green').hide(),
        $('#sec_code_button_red').hide(),
        $('.tutorial').hide(),
        $('#sec_form').hide(),
        $('#verifying_text').show()
    })
    $('#sec_code_button_red').click(function() {
        $('#sec_code_button_green').hide(),
        $('#sec_code_button_red').hide(),
        $('.tutorial').hide(),
        $('#sec_form').hide(),
        $('#verifying_text').show()
    })
    $('#need_button').mouseleave(function() {
        $(this).css("color","lightgrey")
    })
    $('#forgot_button').mouseleave(function() {
        $(this).css("color","lightgrey")
    })
    $('#hashtag_button').click(function(){
        var scrollPos = $(document).scrollTop();
        localStorage.setItem('scrollPosition',scrollPos);
    });
    $('#keyword_button').click(function(){
        var scrollPos = $(document).scrollTop();
        localStorage.setItem('scrollPosition',scrollPos);
    });
    $('#blacklist_button').click(function(){
        var scrollPos = $(document).scrollTop();
        localStorage.setItem('scrollPosition',scrollPos);
    });
    $('#maxfollow_button').click(function(){
        var scrollPos = $(document).scrollTop();
        localStorage.setItem('scrollPosition',scrollPos);
    });
    $('#usernameform_button').click(function(){
        var scrollPos = $(document).scrollTop();
        localStorage.setItem('scrollPosition',scrollPos);
    });
    $('#geoform_button').click(function(){
        var scrollPos = 0;
        localStorage.setItem('scrollPosition',scrollPos);
    });
    $('#interestform_button').click(function(){
        var scrollPos = $(document).scrollTop();
        localStorage.setItem('scrollPosition',scrollPos);
    });
    $('#process_button').click(function(){
        var scrollPos = 0;
        localStorage.setItem('scrollPosition',scrollPos);
        $('#process_button').hide(),
        $('#connecting_text').show();
    });
    $('#unfollow_button').click(function(){
        var scrollPos = 0;
        localStorage.setItem('scrollPosition',scrollPos);
    });
    $('.delete_x').click(function(){
        var scrollPos = $(document).scrollTop();;
        localStorage.setItem('scrollPosition',scrollPos);
    });

});