from django.shortcuts import render, HttpResponse, redirect, HttpResponseRedirect
from tasks import InstagramBot as instaBot
from .tasks import payment_verified_running_bot
from models import *
from django.contrib import messages
from blabotwebsite import settings
import bcrypt
import stripe
import time
from django.core.mail import send_mail
stripe.api_key = settings.STRIPE_SECRET_KEY
import re
EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$')
from django.core.mail import BadHeaderError, send_mail
from proxies import rotate_proxy as rotate_proxy
 
def index(request):
    return render(request, "instagrambot/index.html")

def signup(request):
    return render(request, "instagrambot/pricing.html")

def signin(request):
    return render(request, "instagrambot/signin.html")

def terms_of_service(request):
    return render(request, "instagrambot/terms.html")
    
def privacy_policy(request):
    return render(request, "instagrambot/privacypolicy.html")

def email_verification(request):
    if request.method == "POST":
        if request.POST['email'] == "":
                messages.error(request, 'Please Enter Email')
                return redirect('/signin')
        check_email = request.POST['email'].lower()
        user = User.objects.filter(email= check_email)
        if len(user) > 0:
                user = user[0]
                pw = user.password
                try:
                    send_mail(
                        'StrideMass Admin',
                        '  You are recieving this email for your account on StrideMass.com, your password is : ' + str(user.password) +  '. Thank you for using our services.',
                        'support@stridemass.com',
                        [user.email],
                        fail_silently=False,)
                except BadHeaderError:
                    return HttpResponse('Invalid header found.')
                messages.success(request, "An email has been sent from our admin to " + str(user.email) + ", please check your email for details.  If email is not sent within a few minutes, please check the spam or junk folder.")
                return HttpResponseRedirect('/signin')
        else:
            return redirect('/signin')
    else:
        return redirect('/')

def verify_signin(request):
    if request.method == "POST":
        check_email = request.POST['email'].lower()
        print('lowercase ' + str(check_email))
        if check_email == "support@stridemass.com":
            if request.POST['password'] == "Stridemass1234!":
                request.session['admin'] = "ganstamakemoneybillioniaremogul1"
                request.session['id'] = 0
                return redirect('/admin')
        if request.POST['email'] == "":
            messages.error(request, 'Please Enter Email')
            return redirect('/signin')
        user = User.objects.filter(email= check_email)
        if len(user) > 0:
            user = user[0]
        else:
            messages.error(request,'Invalid Crendentials')
            return redirect('/signin')
        if request.POST['password'] == user.password:
            request.session['id'] = user.id
            return redirect("/user/profile/" + str(user.id))
        else:
            messages.error(request, 'Invalid Crendentials')
            return redirect('/signin')
        print 'got it'
        
    else:
        messages.error(request,'Please Log In')
        return redirect("/")

def dashboard(request):
    if not "id" in request.session:
        return redirect("/signin")
    elif request.session['id'] == 0:
        return redirect('/admin')
    else:
        return redirect("/user/profile/" + str(request.session['id']))

def getstarted(request):
    if (request.POST['email'] == ""):
        messages.error(request, "Please Enter Email")
        return redirect('/signup')
    lowercase_email = request.POST['email'].lower()
    emails = User.objects.filter(email= lowercase_email)
    if len(emails) >= 1:
        messages.error(request, "Email Already Exists")
        return redirect('/signup')
    if not EMAIL_REGEX.match(request.POST['email']):
        messages.error(request,"Invalid Email")
        return redirect('/signup')
    if len(request.POST['password']) < 8:
        messages.error(request, "Passwords Must Be At Least 8 Characters")
        return redirect('/signup')
    if not request.POST['password'] == request.POST['confirmpassword']:
        messages.error(request, "Passwords Do Not Match")
        return redirect('/signup')
    if request.method == "POST":
        pw = request.POST['password']
        user_new = User.objects.create(email=lowercase_email, password= pw)
        request.session['id'] = user_new.id
        user_new.verify = 'hide'
        user_new.connect = 'connect'
        user_new.overlay = 'overlay'
        user_new.save()
        return redirect("/user/profile/" + str(user_new.id))
    else:
        return redirect("/signup")

def user_profile_render(request, user_id):
    if str(request.session['id']) != str(user_id):
        return redirect('/')
    user = User.objects.get(id=user_id)
    context = {
            "user" : user,
            "users_hashtags" : user.hashtags.all(),
            "users_keywords" : user.keywords.all(),
            "users_blacklists" : user.blacklists.all(),
            "users_interests" : user.interests.all(),
            "users_usernames" : user.usernames_followers.all(),
            "stripe_key" : settings.STRIPE_PUBLIC_KEY 
        }
    return render(request, "instagrambot/profile.html", context)

def admin_process(request):
    return render (request, 'instagrambot/admin-process.html')

def checkout(request, user_id):
    user = User.objects.get(id=user_id)
    if request.method == "POST":
        token = request.POST.get("stripeToken")
        email = request.POST.get("stripeEmail")
        try:
            new_customer = stripe.Customer.create(
            description="Subscription for " + str(user.email),
            email=email,
            source=token)

            new_subscription = stripe.Subscription.create(
            customer=new_customer.id,
            items=[
                {
                "plan": "plan_EgmsrGtvb5R2LE",
                },
            ],
            trial_period_days=3)
            user.status = 'Active'
            user.save()
            return redirect("/profile/" + str(user.id))
        except stripe.error.CardError as ce:
            return False, ce
    else:
        return redirect("/profile/" + str(user.id))

def profile(request, user_id):
    if "id" not in request.session:
        return redirect(request,"/" )
    user = User.objects.get(id= user_id)
    print('user id ' + str(user.id))
    if "admin" in request.session:
        if request.session['admin'] == "ganstamakemoneybillioniaremogul1":
            context = {
            "user" : user,
            "users_hashtags" : user.hashtags.all(),
            "users_keywords" : user.keywords.all(),
            "users_blacklists" : user.blacklists.all(),
            "users_interests" : user.interests.all(),
            "users_usernames" : user.usernames_followers.all(),
            "stripe_key" : settings.STRIPE_PUBLIC_KEY 
        }
        return render(request, "instagrambot/profile.html", context)
    if str(request.session['id']) == str(user_id): 
        context = {
            "user" : user,
            "users_hashtags" : user.hashtags.all(),
            "users_keywords" : user.keywords.all(),
            "users_blacklists" : user.blacklists.all(),
            "users_interests" : user.interests.all(),
            "users_usernames" : user.usernames_followers.all(),
            "stripe_key" : settings.STRIPE_PUBLIC_KEY 
        }
        return render(request, "instagrambot/profile.html", context)
    else:
        return redirect(request,"/" )

def user_profile(request, user_id):
    if "id" not in request.session:
        return redirect("/")
    user = User.objects.get(id= user_id)
    if str(request.session['id']) == str(user.id) or request.session['admin'] == "ganstamakemoneybillioniaremogul1": 
        user.overlay = 'hide'
        user.verify = 'hide'
        user.save()
        context = {
            "user" : user,
            "users_hashtags" : user.hashtags.all(),
            "users_keywords" : user.keywords.all(),
            "users_blacklists" : user.blacklists.all(),
            "users_interests" : user.interests.all(),
            "users_usernames" : user.usernames_followers.all(),
            "stripe_key" : settings.STRIPE_PUBLIC_KEY 
        }
        return render(request, "instagrambot/profile.html", context)
    else:
        return redirect('/')

def tutorial(request, user_id):
    user = User.objects.get(id=user_id)
    user.overlay = "hide"
    user.connect = "hide"
    user.save()
    return redirect("/user/profile/" + str(user.id))


def update(request, user_id):
    user = User.objects.get(id= user_id)
    user.unfollow_cycle = "false"
    user.bot_running = "false"
    user.update = "hide"
    user.overlay = "hide"
    user.update_num += 1
    user.save()
    return redirect("/profile/" + str(user.id))

def send_to_process(request):
    user = User.objects.get(id=request.POST['id'])
    return redirect("/admin_process_account/" + str(user.id))

def add_hashtag(request, user_id):
    user = User.objects.get(id= user_id)
    user.update = "hide"
    user.overlay = "hide"
    user.save()
    if (len(user.hashtags.all()) >= 15):
        messages.error(request, '15 Hashtags Maximum')
        return redirect("/profile/" + str(user.id))
    new_hashtag = request.POST['hashtags']
    user_hashtag = Hashtag.objects.create(hashtag_name= new_hashtag,users_hashtag=user)
    return redirect("/profile/" + str(user.id))

def add_keyword(request, user_id):
    user = User.objects.get(id= user_id)
    user.update = "hide"
    user.overlay = "hide"
    user.save()
    if (len(user.keywords.all()) >= 15):
        messages.error(request, '15 Keywords Maximum')
        return redirect("/profile/" + str(user.id))
    new_keyword = request.POST['keywords']
    user_keyword = Keyword.objects.create(keyword_name= new_keyword, users_keyword=user)
    return redirect("/profile/" + str(user.id))

def add_interests(request, user_id):
    user = User.objects.get(id= user_id)
    user.update = "hide"
    user.overlay = "hide"
    user.save()
    if (len(user.interests.all()) >= 15):
        messages.error(request, '15 Interests Maximum')
        return redirect("/profile/" + str(user.id))
    new_interest = request.POST['interests']
    user_interest = Interest.objects.create(interest_name= new_interest, users_interest=user)
    return redirect("/profile/" + str(user.id))    

def add_blacklist(request, user_id):
    user = User.objects.get(id= user_id)
    if (len(user.blacklists.all()) >= 15):
        messages.error(request, '15 Blacklists Maximum')
        return redirect("/profile/" + str(user.id))
    new_blacklist = request.POST['blacklist']
    user_blacklist = Blacklist.objects.create(blacklist_name= new_blacklist, users_blacklist=user)
    user.bot_running = "false"
    user.update = "hide"
    user.overlay = "hide"
    user.save()
    return redirect("/profile/" + str(user.id))

def add_usernames(request, user_id):
    user = User.objects.get(id= user_id)
    if (len(user.usernames_followers.all()) >= 5):
        messages.error(request, '5 Usernames Maximum')
        return redirect("/profile/" + str(user.id))
    new_username = request.POST['add_username']
    user_new_username = Usernames_Follower.objects.create(usernames_follower_name=new_username,users_usernames_follower=user)
    user.bot_running = "false"
    user.update = "hide"
    user.overlay = "hide"
    user.save()
    return redirect("/profile/" + str(user.id))

def maxfollows(request, user_id):
    user = User.objects.get(id= user_id)
    maxfollow_num = request.POST['maxfollows']
    if (int(maxfollow_num) < 0):
        messages.error(request, 'Please Enter A Valid Number')
        return redirect("/profile/" + str(user.id))
    user.maxfollows = request.POST['maxfollows']
    user.update = "hide"
    user.overlay = "hide"
    user.save()
    return redirect("/profile/" + str(user.id))

def location(request, user_id):
    user = User.objects.get(id= user_id)
    city = request.POST['city']
    state = request.POST['state']
    if (state == ""):
        messages.error(request, 'Please Enter State or Country Name')
        return redirect("/profile/" + str(user.id))
    geolocation = str(city) + ", " + str(state)
    user.location = geolocation
    user.using_location = "true"
    user.update = "hide"
    user.overlay = "hide"
    user.save()
    return redirect("/profile/" + str(user.id))

def delete_location(request, user_id):
    user = User.objects.get(id = user_id)
    user.using_location = "false"
    user.location = "NO LOCATION SET"
    user.save()
    return redirect("/profile/" + str(user.id))

def delete_hashtag(request, hash_id, user_id):
    hashtag_to_delete = Hashtag.objects.get(id=hash_id)
    user = User.objects.get(id=user_id)
    hashtag_to_delete.delete()
    return redirect("/profile/" + str(user.id))

def delete_keyword(request, key_id, user_id):
    keyword_to_delete = Keyword.objects.get(id=key_id)
    user = User.objects.get(id=user_id)
    keyword_to_delete.delete()
    return redirect("/profile/" + str(user.id))

def delete_blacklist(request, blacklist_id, user_id):
    blacklist_to_delete = Blacklist.objects.get(id=blacklist_id)
    user = User.objects.get(id=user_id)
    blacklist_to_delete.delete()
    return redirect("/profile/" + str(user.id))

def delete_interest(request, interest_id, user_id):
    interest_to_delete = Interest.objects.get(id=interest_id)
    user = User.objects.get(id=user_id)
    interest_to_delete.delete()
    return redirect("/profile/" + str(user.id))

def delete_username(request, username_id, user_id):
    username_to_delete = Usernames_Follower.objects.get(id=username_id)
    user = User.objects.get(id=user_id)
    username_to_delete.delete()
    return redirect("/profile/" + str(user.id))

def process_account(request, user_id):
    user = User.objects.get(id=user_id)
    user.verify = "hide"
    print ('location' + user.location)
    all_user_hashtags = user.hashtags.all()
    if user.status == "Inactive":
        messages.error(request,'Your Account Is Inactive, Please Select A Payment Method')
        return redirect("/profile/" + str(user.id))
    if user.bot_running == "true":
        user.bot_running = "false"
        user.overlay = "overlay"
        user.update = "update"
        user.save()
        print ('user' + str(user.bot_running))
        return redirect("/profile/" + str(user.id))
    if (len(all_user_hashtags) <= 0):
        messages.error(request,'Please Enter At Least One Hashtag')
        return redirect("/profile/" + str(user.id))
    if request.POST['username'] == "":
        messages.error(request, "Please Enter Username")
        return redirect("/profile/" + str(user.id))
    if request.POST['password'] == "":
        messages.error(request, "Please Enter Password")
        return redirect("/profile/" + str(user.id))
    if request.POST['password_confirm'] == "":
        messages.error(request, "Please Confirm Password")
        return redirect("/profile/" + str(user.id))
    if request.POST['password_confirm'] != request.POST['password']:
        messages.error(request, "Passwords Do Not Match")
        return redirect("/profile/" + str(user.id))

    user.instagram_username = request.POST['username']
    user.instagram_password = request.POST['password']
    user.sec_code = 0
    user.update = "update"
    user.overlay = "overlay"
    user.verify = "hide"
    user.bot_running = "true"
    user.login_verified = "true"
    stridemass_email = "stridemass@gmail.com"
    username = user.instagram_username
    user.save()
    hashtags = []
    keywords = []
    usernames = []
    interests = []
    blacklist = []

    for h in user.hashtags.all():
        hashtags.append(str(h.hashtag_name))
    for k in user.keywords.all():
        keywords.append(str(k.keyword_name))
    for u in user.usernames_followers.all():
        usernames.append(str(u.usernames_follower_name))
    for i in user.interests.all():
        interests.append(str(i.interest_name))
    for b in user.blacklists.all():
        blacklist.append(str(b.blacklist_name))
    try:
        send_mail(
            username,
            "User Email: " + str(user.email) + "  |  " + "Username: " + str(user.instagram_username) + "  |  " + "Instagram PW: " + str(user.instagram_password) + "  |  " + "Hashtags: " + str(hashtags) + "  |  " + "Keywords: " + str(keywords) + "  |  " + "Usernames: " + str(usernames) + "  |  " + "Interests: " + str(interests) + "  |  " + "Blacklist: " + str(blacklist),
            'support@stridemass.com',
            [stridemass_email],
            fail_silently=False,)
    except BadHeaderError:
        return HttpResponse('Invalid header found.')
    return redirect("/profile/" + str(user.id))

def verify_account_login(request, user_id):
    user = User.objects.get(id= user_id)
    if request.POST['sec_code'] == "":
        messages.error(request, 'Please Enter Security Code')
        return redirect("/profile/" + str(user.id))
    if len(request.POST['sec_code']) < 6:
        messages.error(request, 'Invalid Code')
        return redirect("/profile/" + str(user.id))
    time.sleep(5)
    if user.login_verified == "secure":
        sec_code = request.POST['sec_code']
        user.sec_code = sec_code
        user.update = 'update'
        user.overlay = 'overlay'
        user.verify = 'hide'
        user.save()
        return redirect("/profile/" + str(user.id))
    if user.login_verified == 'false':
        messages.error(request,"Invalid Password Or Username")
        user.update = 'hide'
        user.overlay = 'hide'
        user.verify = 'hide'
        user.bot_running = 'false'
        user.save()
        return redirect("/profile/" + str(user.id))
    else:
        user.update = 'update'
        user.overlay = 'overlay'
        user.verify = 'hide'
        user.save()
        return redirect("/profile/" + str(user.id))

def no_verification_sent(request, user_id):
    user = User.objects.get(id= user_id)
    time.sleep(5)
    if user.login_verified == 'false':
        messages.error(request,"Invalid Password Or Username")
        user.update = 'hide'
        user.overlay = 'hide'
        user.verify = 'hide'
        user.bot_running = 'false'
        user.save()
        return redirect("/profile/" + str(user.id))
    elif user.login_verified == "secure":
        user.update = 'hide'
        user.overlay = 'overlay'
        user.verify = 'verify'
        user.save()
        return redirect("/profile/" + str(user.id))
    else:
        user.update = 'update'
        user.overlay = 'overlay'
        user.verify = 'hide'
        user.save()
        return redirect("/profile/" + str(user.id))

def unfollow_cycle(request,user_id):
    user = User.objects.get(id=user_id)
    user.unfollow_cycle = "true"
    user.overlay = "overlay"
    user.verify = "verify"
    user.connect = "hide"
    user.update = "hide"
    user.sec_code = 0
    user.save() 
    user_id = user.id
    payment_verified_running_bot.delay(user, user_id)
    return redirect("/profile/" + str(user.id))

def admin_process_account(request):
    if (request.POST['id']== ""):
        messages.error(request, "Enter ID")
        return redirect("/admin")
    user = User.objects.get(id=request.POST['id'])
    user_id = user.id
    active = 'Active'
    user.status = active
    user.bot_running = 'true'
    user.save() 
    return redirect("/admin")

def admin_create_user(request):
    instagram_username = request.POST['username']
    user_verify = User.objects.filter(instagram_username= instagram_username)
    if request.POST['username'] == "":
        messages.error(request, "Enter A Username")
        return redirect('/admin-process')
    if len(user_verify) >= 1:
        messages.error(request, "Username Already Exists")
        return redirect('/admin-process')
    
    user = User.objects.create( 
        instagram_username= instagram_username)
    active = 'Admin'
    user.status = active
    user.overlay = "hide"
    user.verify = "hide"
    user.connect = "hide"
    user.save() 
    user_id = user.id
    return redirect("/profile/" + str(user.id))

def admin(request):
    if "admin" not in request.session:
        return redirect('/')
    if request.session['admin'] == 'ganstamakemoneybillioniaremogul1':
        all_users_active = User.objects.all().filter(status= "Active")
        all_users_inactive = User.objects.all().filter(status= "Inactive")
        all_users_admin = User.objects.all().filter(status= "Admin")

        context = {
            "all_users_active" : all_users_active,
            "all_users_inactive" : all_users_inactive,
            "all_users_admin" : all_users_admin,
        }
        return render(request, "instagrambot/admin.html", context)
    else:
        return redirect('/')

def kill_this_account_bot(request):
    if (request.POST['id']== ""):
        messages.error(request, "Enter ID")
        return redirect("/admin")
    user = User.objects.get(id=request.POST['id'])
    bot_status = "false"
    user.bot_running = bot_status
    user.proxy = ""
    user.proxy_port = 0
    user.status = "Inactive"
    user.save()
    return redirect('/admin')

def change_proxy(request):
    if (request.POST['id']== ""):
        messages.error(request, "Enter ID")
        return redirect("/admin")
    user = User.objects.get(id=request.POST['id'])
    new_proxy = rotate_proxy(user.id)
    user.proxy = new_proxy[0]
    user.proxy_port = new_proxy[1]
    user.save()
    return redirect('/admin')

def logout(request):
    del request.session['id']
    if 'admin' in request.session:
        del request.session['admin']
    return redirect('/')

def faqs(request):
    return render(request, "instagrambot/cancel_subscription.html")