
from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^', include('apps.instagrambot.urls')),
    url('^', include('django.contrib.auth.urls')),
]
